<?php

class File
{
	private $file_path;


	public function __construct($file_path) // проверка существования файла
	{
		if (file_exists($file_path)) {
			$this->file_path = $file_path;
		} else {
			throw new Exception('File not exists'); // исключение
		}
	}

	function getFileName()
	{
		return basename($this->file_path);  // получение имени файла, отбрасівается путь
	}

	function getSize()
	{
		return $this->prettifySize(filesize($this->file_path)); // полцчение размера файла
	}

	public function getIsReadable()
	{
		return is_readable($this->file_path); // Определяет существование файла и доступен ли он для чтения
	}

	public function getIsWritable()
	{
		return is_writable($this->file_path);
	}

	public function getCreatedDate($format = 'Y-m-d H:i:s')
	{
		return date($format, filectime($this->file_path)); // дата создания файла
	}

	public function getLastEditedDate($format = 'Y-m-d H:i:s')
	{
		return date($format, filemtime($this->file_path)); // дата последнего изменения
	}

	private function prettifySize($bytes) // преобразования размера файла в байтах в другие величины
	{
		switch ($bytes) {
			case $bytes >= 1073741824:
				$size = number_format($bytes / 1073741824) . ' GB';
				break;
			case $bytes >= 1048576:
				$size = number_format($bytes / 1048576, 2) . ' MB';
				break;
			case $bytes >= 1024:
				$size = number_format($bytes / 1024, 2) . ' kB';
				break;
			case $bytes > 1:
				$size = $bytes . ' bytes';
				break;
			case $bytes == 1:
				$size = '1 byte';
				break;
			default:
				$size = '0 bytes';
		}
		return $size;
	}
}